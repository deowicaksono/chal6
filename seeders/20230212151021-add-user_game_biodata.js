'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('User_Game_Biodata', [
    {
      biodata_id:1,
      name:"Deo Wicaksono",
      age:25,
      user_id:1,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      biodata_id:2,
      name:"Budi Setiono",
      age:50,
      user_id:2,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      biodata_id:3,
      name:"Diffa Ramattar",
      age:30,
      user_id:3,
      createdAt: new Date(),
      updatedAt: new Date()
    }
    ], {})
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {

    await queryInterface.bulkDelete('User_Game_Biodata', null, {})
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
