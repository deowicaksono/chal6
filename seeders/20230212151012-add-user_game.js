'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {

    await queryInterface.bulkInsert('User_Games',[
      {
        user_id:1,
        username:'deo',
        password:'deo',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id:2,
        username:'aldo',
        password:'ludo',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id:3,
        username:'diffa',
        password:'kribo',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id:4,
        username:'emon',
        password:'emonyet',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id:5,
        username:'sego',
        password:'jagung',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {})
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('User_Games', null, {})
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
