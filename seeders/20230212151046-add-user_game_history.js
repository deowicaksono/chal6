'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {

    await queryInterface.bulkInsert('User_Game_Histories', [
      {
        history_id:1,
        game:"Tetris",
        score:350,
        user_id:1,
        createdAt: new Date(),
        updatedAt:new Date()
      },
      {
        history_id:2,
        game:"Ludo",
        score:500,
        user_id:1,
        createdAt: new Date(),
        updatedAt:new Date()
      },
      {
        history_id:3,
        game:"Space Invader",
        score:839,
        user_id:1,
        createdAt: new Date(),
        updatedAt:new Date()
      },
      {
        history_id:4,
        game:"Tetris",
        score:430,
        user_id:2,
        createdAt: new Date(),
        updatedAt:new Date()
      },
      {
        history_id:5,
        game:"Ludo",
        score:300,
        user_id:2,
        createdAt: new Date(),
        updatedAt:new Date()
      },
    ], {})
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {

    await queryInterface.bulkDelete('User_Game_Histories', null, {})
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
