'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_Game_History extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  User_Game_History.init({
    history_id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
  },
    game: DataTypes.STRING,
    score: DataTypes.INTEGER,
    user_id: {
      type:DataTypes.INTEGER,
      references:{model:'User_Games', key:'user_id'}
    }
  }, {
    sequelize,
    modelName: 'User_Game_History',
  });
  return User_Game_History;
};