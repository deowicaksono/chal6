var express=require('express');
var router = express.Router();

var {User_Game} = require('../models');


router.get('/user_game', async(req,res) =>{
    const result = await User_Game.findAll({raw:true});
    res.render('adminpanel',{data:result});
})

router.post('/user_game', async(req,res) =>{
    const {user_id,username,password} = req.body;
    const {method}=req.query;
    if(!method){
        await User_Game.create({
            username:username,
            password:password
        })
    }else if(method == "PUT"){
        //masuk method edit
        await User_Game.update(
            {username, password,},
            {where:{user_id:parseInt(user_id)}}
        )

    }

    res.redirect('/user_game');
})

router.post('/user_game/delete', async function(req,res){
    const {id}=req.query;

    const result=await User_Game.destroy({
        where: {user_id:id}
    })
    res.redirect('/user_game');
})

router.post('/user_game/edit', async function (req,res){
    const {id}=req.query;

    const dataToEdit=await User_Game.findOne({where:{user_id:id}, raw:true})
    console.log("Data yang akan diedit:", dataToEdit);
    
    res.render("edit",{dataToEdit})
})


module.exports=router;
