var express=require('express');
var router = express.Router();


let dataAdmin=require('../adminusers.json');
const delay = ms => new Promise(res => setTimeout(res, ms));
let loginFailed=false;

router.get('/login', (req,res) =>{
    res.status(200).render('login',{loginFailed:loginFailed});
})

router.post('/login', async(req,res) =>{
    const {username, password} = req.body;
    console.log(`ini username:` + username);
    console.log('ini password:' + password);
    console.log('Initiate find data admin');
    const adminFound=dataAdmin.find(item =>((item.username==username) && (item.password==password)));
    if(!adminFound){
        console.log("user not found")
        loginFailed=true;
        res.redirect('/login')
    }
    else{
        console.log("Welcome back Admin");
        loginFailed=false;
        res.redirect('/user_game')

    }
})

module.exports=router;