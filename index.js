const express = require('express');
const app= express();
const port = 8000;
const fs = require('fs');

app.set('view engine','ejs');
app.use(express.urlencoded({extended: true}))
app.use(express.json());
app.use(express.static('public'))

let routeUserGame= require('./routes/user_game.js');
let routeLogin = require('./routes/login.js');
app.use(routeLogin);
app.use(routeUserGame);


app.get("/", (req,res) => {
    res.status(200).redirect('/login')
})




app.get('*', (req,res)=>{
    res.status(404).send("page doesnt exist");
})

app.listen(port, () => console.log('example app currently listening'));